#!/usr/bin/env python3

import argparse
import os
from multiprocessing import Pool

from PIL import Image, ImageOps


def process(src_fname_path, dest_fname_path, cutoff):
    try:
        img = Image.open(src_fname_path)
        img = ImageOps.grayscale(img)
        img = ImageOps.autocontrast(img, cutoff=cutoff, ignore=None)
        img.save(dest_fname_path)
        print('{0} -> {1}'.format(src_fname_path, dest_fname_path))
    except Exception as e:
        print('processing {0}: {1}'.format(src_fname_path, e))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('src_dir', help='Source directory')
    parser.add_argument('dest_dir', help='Destination directory')
    parser.add_argument('--cutoff', type=int, default=10, help='Cutoff')
    parser.add_argument('--n', type=int, help='number of parallel threads', default=4)
    args = parser.parse_args()

    if args.src_dir == args.dest_dir:
        print('source and destination dir must be different!')
        exit(1)

    if not os.path.exists(args.dest_dir):
        os.makedirs(args.dest_dir)

    process_args = []
    for src_fname in os.listdir(args.src_dir):
        src_fname_path = os.path.join(args.src_dir, src_fname)
        dest_fname_path = os.path.join(args.dest_dir, src_fname)
        process_args.append((src_fname_path, dest_fname_path, args.cutoff))

    with Pool(args.n) as p:
        p.starmap(process, process_args)

    exit(0)

if __name__ == '__main__':
    main()
