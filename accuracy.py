#!/usr/bin/env python3

import argparse
import re

from collections import defaultdict
from hashlib import sha1

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('output_file', help='classifier output', type=str)
    parser.add_argument('--stats', action='store_true')
    args = parser.parse_args()

    total = 0
    correct = 0
    symbol_totals = defaultdict(lambda: 0)
    symbol_accuracy = defaultdict(lambda: 0)

    # TODO: handle scrambled output
    expr = re.compile(r'^\s*(\S+?)(?:_\d+)?\.png\,\s*(\S+)\s*$')
    sha1_expr = re.compile(r'^[a-zA-Z0-9]{40}$')

    at_least_one_scrambled = False

    with open(args.output_file) as f:
        for line in f:
            if line.isspace():
                continue
            
            matches = expr.search(line)
            if not matches:
                continue

            expected, actual = matches.groups()
            scrambled = re.search(sha1_expr, expected)
            if scrambled:
                at_least_one_scrambled = True
                actual_hash = sha1(actual.encode('utf-8')).hexdigest()
                correct += (expected == actual_hash)
            else:
                correct += (expected == actual)
            total += 1

            if not args.stats:
                continue

            for c1, c2 in zip(expected, actual):
                symbol_totals[c1] += 1
                if c1 != c2:
                    continue
                symbol_accuracy[c1] += 1

    pct = correct / total * 100 if total else 0.0
    print('{0}/{1} {2:0.2f}%'.format(correct, total, pct))
    if not args.stats:
        exit(0)
    elif at_least_one_scrambled:
        print('Cannot provide per-symbol accuracy due to scrambled filenames')
        exit(0)

    print('Per-Symbol Accuracy:')
    for k in sorted(symbol_totals.keys()):
        if symbol_totals[k] == 0:
            pct = 'NaN'
        else:
            pct = symbol_accuracy[k] / symbol_totals[k] * 100.0
        print('\t{0}: {1}/{2} {3:.2f}%'.format(
            k,
            symbol_accuracy[k],
            symbol_totals[k],
            pct,
        ))


if __name__ == '__main__':
    main()
