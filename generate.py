#!/usr/bin/env python3

import os
import numpy
import random
import string
import cv2
import argparse
import captcha.image
import subprocess

from multiprocessing import Pool

def scramble_image_name(image_name):
    import hashlib
    m = hashlib.sha1()
    m.update(image_name.encode('utf-8'))
    return m.hexdigest()


class SimpleAudioCaptcha(object):
    def __init__(self, voicedir):
        self._files = {}
        for dirname in os.listdir(voicedir):
            if len(dirname) != 1:
                continue
            subdir = os.path.join(voicedir, dirname)
            if not os.path.isdir(subdir):
                continue
            choice_files = []
            for filename in os.listdir(subdir):
                fullpath = os.path.join(subdir, filename)
                if not (fullpath.endswith('.wav') or fullpath.endswith('.mp3')):
                    continue
                if not os.path.isfile(fullpath):
                    continue
                choice_files.append(fullpath)
            self._files[dirname] = choice_files

    def write(self, chars, output):
        files = []
        for c in chars:
            files.append(random.choice(self._files[c]))
        cmd = ['sox'] + files + [output]
        subprocess.call(cmd)


def mkcaptcha(captcha_generator, captcha_text, scramble=False, audio=False, output_dir='.'):
    captcha_name_scrambled = captcha_text
    if scramble:
        captcha_name_scrambled = scramble_image_name(captcha_text)

    if audio:
        ext = '.mp3'
    else:
        ext = '.png'
    captcha_path = os.path.join(output_dir, captcha_name_scrambled + ext)
    if os.path.exists(captcha_path):
        version = 1
        while os.path.exists(os.path.join(output_dir, captcha_name_scrambled + '_' + str(version) + ext)):
            version += 1
        captcha_path = os.path.join(output_dir, captcha_name_scrambled + '_' + str(version) + ext)

    if audio:
        captcha_generator.write(captcha_text, captcha_path)
    else:
        image = numpy.array(captcha_generator.generate_image(captcha_text))
        cv2.imwrite(captcha_path, image)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--width', help='Width of captcha image', type=int)
    parser.add_argument('--height', help='Height of captcha image', type=int)
    parser.add_argument('--length', help='Length of captchas in characters', type=int)
    parser.add_argument('--count', help='How many captchas to generate', type=int)
    parser.add_argument('--scramble', help='Whether to scramble image names', default=False, action='store_true')
    parser.add_argument('--output-dir', help='Where to store the generated captchas', type=str)
    parser.add_argument('--symbols', help='File with the symbols to use in captchas', type=str)
    parser.add_argument('--seed', help='Seed for the captcha generator', type=str)
    parser.add_argument('--audio', help='Generate audio captchas', action='store_true')
    parser.add_argument('-n', help='parallelism', type=int, default=4)
    args = parser.parse_args()

    if args.width is None:
        print("Please specify the captcha image width")
        exit(1)

    if args.height is None:
        print("Please specify the captcha image height")
        exit(1)

    if args.length is None:
        print("Please specify the captcha length")
        exit(1)

    if args.count is None:
        print("Please specify the captcha count to generate")
        exit(1)

    if args.output_dir is None:
        print("Please specify the captcha output directory")
        exit(1)

    if args.symbols is None:
        print("Please specify the captcha symbols file")
        exit(1)

    random_seed = None
    if args.seed is not None:
        random_seed = args.seed.encode('utf-8')

    random.seed(random_seed)

    if args.audio:
        captcha_generator = SimpleAudioCaptcha('voice/')
    else:
        captcha_generator = captcha.image.ImageCaptcha(width=args.width, height=args.height)

    symbols_file = open(args.symbols, 'r')
    captcha_symbols = symbols_file.readline().strip()
    symbols_file.close()

    print("Generating captchas with symbol set {" + captcha_symbols + "}")

    if not os.path.exists(args.output_dir):
        print("Creating output directory " + args.output_dir)
        os.makedirs(args.output_dir)

    pool_args = []
    for i in range(args.count):
        captcha_text = ''.join([random.choice(captcha_symbols) for j in range(args.length)])
        pool_args.append([captcha_generator, captcha_text, args.scramble, args.audio, args.output_dir])

    with Pool(args.n) as p:
        p.starmap(mkcaptcha, pool_args)



if __name__ == '__main__':
    main()
