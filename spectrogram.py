#!/usr/bin/env python3

import argparse
import os

from multiprocessing import Pool

import librosa
import librosa.display
from librosa.effects import time_stretch
from librosa.feature import melspectrogram
from python_speech_features import mfcc

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sg


def melspec(y, sr):
    ms = melspectrogram(y, sr)
    spec_db = librosa.amplitude_to_db(ms)
    return spec_db


def process(src_fname_path, dest_fname_path, width, height):
    if not src_fname_path.endswith('.mp3'):
        return

    try:
        y, sr = librosa.load(src_fname_path)
        
        spec_db = melspec(y, sr)

        # generate plot
        scale = 0.01 # assume 100 pixels per inch to get pixels
        fig = plt.figure(figsize=(width*scale, height*scale))
        plt.box(False)
        plt.subplots_adjust(left=0, right=1, bottom=0, wspace=0, hspace=0)
        librosa.display.specshow(spec_db, sr=sr, cmap='gray_r', fmin=300, fmax=3400)
        fig.savefig(dest_fname_path, bbox_inches=None, pad_inches=0)
        plt.close()
        print('{0} -> {1}'.format(src_fname_path, dest_fname_path))
    except Exception as e:
        print('processing {0}: {1}'.format(src_fname_path, e))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('src_dir', help='Source directory')
    parser.add_argument('dest_dir', help='Destination directory')
    parser.add_argument('--width', default=128, type=int)
    parser.add_argument('--height', default=64, type=int)
    parser.add_argument('-n', help='number to process in parallel', type=int, default=4)
    args = parser.parse_args()

    if args.src_dir == args.dest_dir:
        print('source and destination dir must be different!')
        exit(1)

    if not os.path.exists(args.dest_dir):
        os.makedirs(args.dest_dir)

    process_args = []
    for src_fname in os.listdir(args.src_dir):
        src_fname_path = os.path.join(args.src_dir, src_fname)
        dest_fname = src_fname.replace('.mp3', '.png')
        dest_fname_path = os.path.join(args.dest_dir, dest_fname)
        process_args.append((src_fname_path, dest_fname_path, args.width, args.height))

    with Pool(args.n) as p:
        p.starmap(process, process_args)

    exit(0)

if __name__ == '__main__':
    main()
