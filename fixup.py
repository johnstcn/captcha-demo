#!/usr/bin/env python3

import argparse
import os
import re
import subprocess


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', help='input file', type=str)
    parser.add_argument('output_file', help='output file', type=str)
    parser.add_argument('--captcha-dir', help='captcha dir', type=str, default='.')
    parser.add_argument('--image-viewer', help='image viewer', type=str, default='/usr/bin/catimg')
    args = parser.parse_args()

    expr = re.compile(r'^\s*(\S+?(?:_\d+)?\.png)\,\s*(\S+)\s*$')

    with open(args.input_file) as fin:
        with open(args.output_file, 'w+') as fout:
            for n, line in enumerate(sorted(fin, key=lambda l: l[0])):
                if line.isspace():
                    continue
                
                matches = expr.search(line)
                if not matches:
                    continue

                filename, prediction = matches.groups()
                filename_path = os.path.join(args.captcha_dir, filename)

                subprocess.run([args.image_viewer, filename_path])
                answer = input('\nFile #{0}: {1}\nPredicted: {2}.\nCorrect? [Y/n]: ' \
                            .format(n+1, filename_path, prediction)).strip()
                if answer.lower().strip().startswith('n'):
                    correct = input('Enter correction: ').strip()
                else:
                    correct = prediction

                outline = '{0},{1}\n'.format(filename, correct)
                fout.write(outline)
                fout.flush()


if __name__ == '__main__':
    main()
