#!/usr/bin/env python3

import argparse
import os
import sys

from multiprocessing import Pool

import librosa
import librosa.display
from librosa.effects import time_stretch
from python_speech_features import mfcc

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sg


def mfcc_spec(y, sr, target_sr=16000, num_symbols=4, secs_per_symbol=0.5):
    # resample to 16khz for consistency
    resampled = librosa.resample(y, sr, target_sr, res_type='fft', fix=True, scale=True)
    # we need to ensure a consistent audio length to get a consistent number of cepstra
    num_samples = len(resampled)
    expected_seconds = secs_per_symbol * num_symbols
    expected_samples = target_sr * expected_seconds
    stretch_ratio = num_samples / expected_samples
    stretched = time_stretch(resampled, stretch_ratio)
    frame_size = 0.025
    nframes = expected_seconds / frame_size
    frame_len = expected_samples / nframes
    fft_size = 2 * frame_len
    # now we can perform MFCC extraction
    cepstra = mfcc(stretched, nfft=int(fft_size))
    return cepstra


def resample(y, sr, target_sr=16000):
    # resample to 16khz for consistency
    resampled = librosa.resample(y, sr, target_sr, res_type='fft', fix=True, scale=True)
    return resampled, target_sr


def stretch(y, desired_samples):
    num_samples = len(y)
    stretch_ratio = num_samples / desired_samples
    return time_stretch(y, stretch_ratio)


def process(src_fname_path, dest_fname_path, secs_per_symbol, num_symbols):
    if not src_fname_path.endswith('.mp3'):
        return

    try:
        y, sr = librosa.load(src_fname_path)
        # sys.stdout.write("before resampling: {0} samples\n".format(len(y)))
        # sys.stdout.flush()
        y, sr = resample(y, sr)
        # sys.stdout.write("after resampling: {0} samples\n".format(len(y)))
        # sys.stdout.flush()
        desired_samples = secs_per_symbol * num_symbols * sr
        y = stretch(y, desired_samples)
        # sys.stdout.write("after stretching: {0} samples\n".format(len(y)))
        # sys.stdout.flush()
        cepstra = mfcc(y, sr)
        # sys.stdout.write("after mfcc: {0} features\n".format(len(y)))
        # sys.stdout.flush()
        # generate plot
        scale=0.01
        fig = plt.figure(figsize=(cepstra.shape[1]*scale, cepstra.shape[0]*scale))
        plt.box(False)
        plt.subplots_adjust(left=0, right=1, bottom=0, wspace=0, hspace=0)
        #librosa.display.specshow(spec_db, sr=sr, cmap='gray_r', fmin=300, fmax=3400)
        plt.axis('off')
        plt.imshow(cepstra, cmap='gray_r', interpolation='none')
        fig.savefig(dest_fname_path, bbox_inches=None, pad_inches=0)
        plt.close()
        print('{0} -> {1}'.format(src_fname_path, dest_fname_path))
    except Exception as e:
        print('processing {0}: {1}'.format(src_fname_path, e))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('src_dir', help='Source directory')
    parser.add_argument('dest_dir', help='Destination directory')
    parser.add_argument('--length', default=4, type=int)
    parser.add_argument('--secs_per_symbol', default=0.5, type=float)
    parser.add_argument('-n', help='number to process in parallel', type=int, default=4)
    args = parser.parse_args()

    if args.src_dir == args.dest_dir:
        print('source and destination dir must be different!')
        exit(1)

    if not os.path.exists(args.dest_dir):
        os.makedirs(args.dest_dir)

    process_args = []
    for src_fname in os.listdir(args.src_dir):
        src_fname_path = os.path.join(args.src_dir, src_fname)
        dest_fname = src_fname.replace('.mp3', '.png')
        dest_fname_path = os.path.join(args.dest_dir, dest_fname)
        process_args.append((src_fname_path, dest_fname_path, args.secs_per_symbol, args.length))

    with Pool(args.n) as p:
        p.starmap(process, process_args)

    exit(0)

if __name__ == '__main__':
    main()
