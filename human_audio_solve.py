#!/usr/bin/env python3

import argparse
import csv
import os
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument('output', help='path to results file')
parser.add_argument('--captcha-dir', help='captcha dir', type=str, default='.')
parser.add_argument('--player', help='audio player', type=str, default='/usr/bin/play')
args = parser.parse_args()

output_abspath = os.path.join(os.getcwd(), args.output)
if not os.path.exists(output_abspath):
    open(output_abspath, 'w').close()

with open(output_abspath, 'w') as output:
    for fname in os.listdir(args.captcha_dir):
        if not fname.endswith('.mp3'):
            continue
        answer = ''
        while not answer:
            file_abspath = os.path.join(args.captcha_dir, fname)
            subprocess.run([args.player, file_abspath]) 
            answer = input("What did you hear? >").strip().upper()
        output.write('{0},{1}\n'.format(fname, answer))

