FROM python:3-slim

RUN apt-get update && apt-get install -y --no-install-recommends \ 
    libgtk2.0-dev

WORKDIR /root

ADD Pipfile /root/Pipfile
ADD Pipfile.lock /root/Pipfile.lock

RUN pip3 install pipenv && \
    pipenv install --skip-lock

ADD ./classify.py /root/classify.py
ADD symbols.txt /root/symbols.txt
ADD model.h5 /root/model.h5
ADD model.json /root/model.json


ENTRYPOINT pipenv run /root/classify.py \
	--model-name model \
	--captcha-dir /input \
        --symbols /root/symbols.txt \
	--output /output.txt

